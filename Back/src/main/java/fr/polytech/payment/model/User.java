package fr.polytech.payment.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Classe representant l'entite User en base de donnees
 * @author Thomas Rossi
 * @version 1.0
 */
@Entity
@Table(name = "utilisateur")
public class User implements Serializable{
	
	private static final long serialVersionUID = 1236509010853047217L;

	/**
	 * Id de l'utilsateur au format UUID
	 */
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
	        name = "UUID",
	        strategy = "org.hibernate.id.UUIDGenerator"
	    )
	@Column(name = "id_user", updatable = false, nullable = false)
	private UUID idUser;
	
	@Column(name = "login", nullable = false, unique = true)
	private String login;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ReferencePanier> panier;
	
	public User() {
		panier = new HashSet<>();
	}
	
	public User(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	public UUID getId() {
		return idUser;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
    public boolean equals(Object object) {
        return (object instanceof User) 
             ? login.equals(((User) object).login) 
             : (object == this);
    }
	
	@Override
	public int hashCode() {
		return login.hashCode();
	}

}
