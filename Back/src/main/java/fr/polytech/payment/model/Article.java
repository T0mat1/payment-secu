package fr.polytech.payment.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "article")
public class Article implements Serializable{
	
	private static final long serialVersionUID = -4334414011497699084L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
	        name = "UUID",
	        strategy = "org.hibernate.id.UUIDGenerator"
	    )
	@Column(name = "id_article", updatable = false, nullable = false)
	private UUID idArticle;
	
	@Column(name = "reference", updatable = false, nullable = false, unique = true)
	private String reference;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "stock", nullable = false)
	private Integer stock;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "price", nullable = false)
	private BigDecimal price;
	
	@OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
	private List<ReferencePanier> refs;

	public Article() {
		
	}

	public Article(String reference, String name, Integer stock, String description, BigDecimal price) {
		this.reference = reference;
		this.name = name;
		this.stock = stock;
		this.description = description;
		this.price = price;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the stock
	 */
	public Integer getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return idArticle;
	}
	
	@Override
    public boolean equals(Object object) {
        return (object instanceof Article) 
             ? reference.equals(((Article) object).reference) 
             : (object == this);
    }
	
	@Override
	public int hashCode() {
		return reference.hashCode();
	}
	
}
