package fr.polytech.payment.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ref_panier")
public class ReferencePanier implements Serializable {

	private static final long serialVersionUID = -6173738563265224592L;

	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
	        name = "UUID",
	        strategy = "org.hibernate.id.UUIDGenerator"
	    )
	@Column(name = "id_panier", updatable = false, nullable = false)
	private UUID id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private User owner;
	
	@Column(name = "quantity", nullable = false)
	private Integer quantity;
	
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER, optional = false)
	private Article article;

	public ReferencePanier() {
	}
	
	public ReferencePanier(User owner, Integer quantity, Article article) {
		this.owner = owner;
		this.quantity = quantity;
		this.article = article;
	}

	/**
	 * @return the owner
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the article
	 */
	public Article getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(Article article) {
		this.article = article;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}
	
	@Override
    public boolean equals(Object object) {
        if (object instanceof ReferencePanier) {
        	return owner.equals(((ReferencePanier) object).getOwner()) && article.equals(((ReferencePanier) object).getArticle());
        } else {
        	return object == this;
        } 
    }
	
	@Override
	public int hashCode() {
		return Objects.hash(owner, article);
	}
	
}
