package fr.polytech.payment.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.polytech.payment.model.Article;

public interface ArticleRepository extends JpaRepository<Article, UUID>{
	
	List<Article> findAllByStockGreaterThan(Integer stock);
	Optional<Article> findByReference(String reference);

}
