package fr.polytech.payment.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.polytech.payment.model.User;

public interface UserRepository extends JpaRepository<User, UUID>{
	
	Optional<User> findByLogin(String login);

}
