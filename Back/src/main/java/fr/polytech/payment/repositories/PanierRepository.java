package fr.polytech.payment.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.polytech.payment.model.Article;
import fr.polytech.payment.model.ReferencePanier;
import fr.polytech.payment.model.User;

public interface PanierRepository extends JpaRepository<ReferencePanier, UUID> {

	List<ReferencePanier> findAllByOwner(User owner);
	Optional<ReferencePanier> findByOwnerAndArticle(User owner, Article article);
	
	List<ReferencePanier> deleteByOwner(User owner);
	
}
