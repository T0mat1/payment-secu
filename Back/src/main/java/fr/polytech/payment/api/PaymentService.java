package fr.polytech.payment.api;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.ReferenceAlreadyExists;
import fr.polytech.payment.business.exceptions.ReferenceDoesNotExist;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.business.managers.PaymentManager;
import fr.polytech.payment.mapping.dto.ReferencePanierDTO;
import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.impl.ArticleDTOToArticleMapper;
import fr.polytech.payment.mapping.mappers.impl.ReferencePanierToReferencePanierDTOMapper;
import fr.polytech.payment.model.ReferencePanier;

/**
 * Payment REST Service
 * @author Thomas Rossi
 * @version 1.0
 */
@RestController
@RequestMapping("/payment")
public class PaymentService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);
	private static final String DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE = "Warning, the user {} cannot be found.";
	private static final String DEFAULT_ARTICLE_NOT_FOUND_LOGGER_MESSAGE = "Warning, the article {} does not exist.";
	private static final String DEFAULT_REFERENCE_NOT_FOUND_LOGGER_MESSAGE = "Warning, the reference {} in basket does not exist.";
	
	private static final String DEFAULT_USER_NOT_FOUND_MESSAGE = "User cannot be found.";
	private static final String DEFAULT_ARTICLE_NOT_FOUND_MESSAGE = "The article cannot be found.";
	
	
	private PaymentManager paymentManager;
	
	@Autowired
	public PaymentService(PaymentManager paymentManager) {
		this.paymentManager = paymentManager;
	}
	
	/**
	 * GET method to get all references
	 * @param user the user's data
	 * @return a response entity with basket and the status 200 OK or 400 BAD REQUEST if the user is not found
	 * @see {@link UserDTO}
	 */
	@GetMapping("/basket")
	public ResponseEntity<Object> getAllBasketReferences(@RequestBody UserDTO user) {
		try {
			List<ReferencePanierDTO> basket = paymentManager.updateBasket(user);
			LOGGER.info("Get all items in the basket of user {}", user.getLogin());
			return new ResponseEntity<>(basket, HttpStatus.OK);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, user.getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * DELETE method to empty the basket
	 * @param user the user's data
	 * @return a response entity with the status 200 OK or 400 BAD REQUEST if the user is not found
	 * @see {@link UserDTO}
	 */
	@DeleteMapping("/basket")
	public ResponseEntity<Object> emptyBasket(@RequestBody UserDTO user) {
		try {
			LOGGER.info("Empty the basket of user {}.", user.getLogin());
			paymentManager.emptyBasket(user);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, user.getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * GET method to get the cost
	 * @param user the user's data
	 * @return a response entity with the cost and the status 200 OK or 400 BAD REQUEST if the user is not found
	 * @see {@link UserDTO}
	 */
	@GetMapping("/cost")
	public ResponseEntity<Object> getCost(@RequestBody UserDTO user) {
		try {
			LOGGER.info("Get cost of the basket of user {}.", user.getLogin());
			return new ResponseEntity<>(paymentManager.processCost(paymentManager.checkBasket(user)), HttpStatus.OK);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, user.getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * POST method to process the payment
	 * @param user the user's data
	 * @return a response entity with the status 200 OK or 400 BAD REQUEST if the user is not found or 500 INTERNAL SERVER ERROR
	 * @see {@link UserDTO}
	 */
	@PostMapping("/confirmation")
	public ResponseEntity<Object> confirmPayment(@RequestBody UserDTO user) {
		try {
			LOGGER.info("Confirm the payment of user {}.", user.getLogin());
			paymentManager.processPayment(paymentManager.checkBasket(user));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, user.getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		} catch (IllegalStateException e) {
			LOGGER.warn("The basket does not belong to one user or some articles are no longer available.");
			return new ResponseEntity<>("Error while performing the payment. Payment is cancelled", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * POST method to add a reference
	 * @param ref the reference to add
	 * @return a response entity with the status 200 OK or 400 BAD REQUEST
	 * @see {@link ReferencePanierDTO}
	 */
	@PostMapping("/basket/article")
	public ResponseEntity<Object> addReference(@RequestBody ReferencePanierDTO ref) {
		if (paymentManager.isArticleAvailable(new ArticleDTOToArticleMapper().map(ref.getArticle()), ref.getQuantity())) { //check if article is available
			try {
				LOGGER.info("Add a reference from a basket.");
				paymentManager.addReference(ref);
				return new ResponseEntity<>(HttpStatus.OK);
			} catch (ArticleDoesNotExist e) {
				LOGGER.warn(DEFAULT_ARTICLE_NOT_FOUND_LOGGER_MESSAGE, ref.getOwner().getLogin());
				return new ResponseEntity<>(DEFAULT_ARTICLE_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
			} catch (UserNotFoundBusinessException e) {
				LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, ref.getOwner().getLogin());
				return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
			} catch (ReferenceAlreadyExists e) {
				LOGGER.warn("Cannot update this reference, it already exists in the database.");
				return new ResponseEntity<>("This reference already exists, please consider updating it.", HttpStatus.BAD_REQUEST);
			}
		} else {
			LOGGER.info("The article {} is not available for the command.", ref.getArticle().getReference());
			return new ResponseEntity<>("This article is not available at the moment", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * PUT method to update a reference
	 * @param ref the reference to update
	 * @return a response entity with the status 200 OK or 400 BAD REQUEST
	 * @see {@link ReferencePanierDTO}
	 */
	@PutMapping("/basket/article")
	public ResponseEntity<Object> updateReference(@RequestBody ReferencePanierDTO ref) {
		try {
			LOGGER.info("Update a reference from a basket.");
			paymentManager.updateReference(ref);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (ArticleDoesNotExist e) {
			LOGGER.warn(DEFAULT_ARTICLE_NOT_FOUND_LOGGER_MESSAGE, ref.getOwner().getLogin());
			return new ResponseEntity<>(DEFAULT_ARTICLE_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, ref.getOwner().getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		} catch (ReferenceDoesNotExist e) {
			LOGGER.warn(DEFAULT_REFERENCE_NOT_FOUND_LOGGER_MESSAGE, ref);
			return new ResponseEntity<>("This reference does not exist, please consider adding it.", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * DELETE method to remove a reference
	 * @param ref the reference to delete
	 * @return a response entity with the status 200 OK or 400 BAD REQUEST
	 * @see {@link ReferencePanierDTO}
	 */
	@DeleteMapping("/basket/article")
	public ResponseEntity<Object> deleteReference(@RequestBody ReferencePanierDTO ref) {
		try {
			LOGGER.info("Delete a reference from a basket.");
			paymentManager.removeReference(ref);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (ArticleDoesNotExist e) {
			LOGGER.warn(DEFAULT_ARTICLE_NOT_FOUND_LOGGER_MESSAGE, ref.getOwner().getLogin());
			return new ResponseEntity<>(DEFAULT_ARTICLE_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, ref.getOwner().getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		} catch (ReferenceDoesNotExist e) {
			LOGGER.warn(DEFAULT_REFERENCE_NOT_FOUND_LOGGER_MESSAGE, ref);
			return new ResponseEntity<>("This reference does not exist.", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * GET method to get only the article that are available in the user basket
	 * @param user the user's data
	 * @return a response entity with basket and the status 200 OK or 400 BAD REQUEST if the user is not found
	 * @see {@link UserDTO}
	 */
	@GetMapping("/basket/available")
	public ResponseEntity<Object> getAllAvailableItems(@RequestBody UserDTO user) {
		try {
			LOGGER.info("Get all available items for user {}.", user.getLogin());
			ReferencePanierToReferencePanierDTOMapper mapper = new ReferencePanierToReferencePanierDTOMapper();
			List<ReferencePanierDTO> dtos = new ArrayList<>();
			List<ReferencePanier> checkedBasket = paymentManager.checkBasket(user);
			for (ReferencePanier ref : checkedBasket) {
				dtos.add(mapper.map(ref));
			}
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		} catch (UserNotFoundBusinessException e) {
			LOGGER.warn(DEFAULT_USER_NOT_FOUND_LOGGER_MESSAGE, user.getLogin());
			return new ResponseEntity<>(DEFAULT_USER_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
		}
	}
	
}
