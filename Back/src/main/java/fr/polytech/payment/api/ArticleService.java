package fr.polytech.payment.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.MissingDataException;
import fr.polytech.payment.business.managers.ArticleManager;
import fr.polytech.payment.mapping.dto.ArticleDTO;

/**
 * Article REST Service
 * @author Thomas Rossi
 * @version 1.0
 */
@RestController
public class ArticleService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ArticleService.class);
	
	private static String articleDoesNotExistMessage = "Article {} does not exist.";
	
	private ArticleManager articleManager;
	
	@Autowired
	public ArticleService(ArticleManager articleManager) {
		this.articleManager = articleManager;
	}
	
	/**
	 * GET method to get all articles
	 * @return a response entity with the list of all the articles and the status 200 OK
	 */
	@GetMapping("/articles")
	public ResponseEntity<Object> getAllArticles() {
		LOGGER.info("Get all articles");
		return new ResponseEntity<>(articleManager.getAllArticles(), HttpStatus.OK);
	}
	
	/**
	 * GET method to get all available articles
	 * @return a response entity with the list of all the avalaible articles and the status 200 OK
	 */
	@GetMapping("/articles/available")
	public ResponseEntity<Object> getAllAvailableArticles() {
		LOGGER.info("Get all avalaible articles");
		return new ResponseEntity<>(articleManager.getAllAvailableArticles(), HttpStatus.OK);
	}
	
	/**
	 * GET method to get one article
	 * @param reference the reference of the article
	 * @return a response entity with the article and the status 200 OK or a message and 404 if not found
	 */
	@GetMapping("/article/{reference}")
	public ResponseEntity<Object> getOneArticle(@PathVariable String reference) {
		try {
			LOGGER.info("Get article with reference {}", reference);
			return new ResponseEntity<>(articleManager.getOneArticle(reference), HttpStatus.OK);
		} catch (ArticleDoesNotExist e) {
			LOGGER.info(articleDoesNotExistMessage, reference);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * POST method to create a new article
	 * @param article the data of the article to be created
	 * @return a response entity with the article and the status 201 CREATED or 400 if there is missing data
	 * @see {@link ArticleDTO}
	 */
	@PostMapping("/article")
	public ResponseEntity<Object> createNewArticle(@RequestBody ArticleDTO article) {
		try {
			LOGGER.info("Create new article.");
			return new ResponseEntity<>(articleManager.addArticle(article), HttpStatus.CREATED);
		} catch (MissingDataException e) {
			LOGGER.info("Mendatory data is missing, aborting article creation.");
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * PUT method to update an article
	 * @param article the data of the article to be created
	 * @return a response entity with the article and the status 200 OK, 404 if not found or 400 if there is missing data
	 * @see {@link ArticleDTO}
	 */
	@PutMapping("/article")
	public ResponseEntity<Object> updateArticle(@RequestBody ArticleDTO article) {
		try {
			LOGGER.info("Updating article {}", article.getReference());
			return new ResponseEntity<>(articleManager.updateArticle(article), HttpStatus.OK);
		} catch (ArticleDoesNotExist e) {
			LOGGER.info(articleDoesNotExistMessage, article.getReference());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (MissingDataException e) {
			LOGGER.info("Mendatory data is missing, aborting article update.");
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * DELETE method to delete an article
	 * @param reference the reference of the article
	 * @return a response entity with the article and the status 200 OK, 404 if not found
	 */
	@DeleteMapping("/article/{reference}")
	public ResponseEntity<HttpStatus> deleteArticle(@PathVariable String reference) {
		try {
			LOGGER.info("Delete article {}", reference);
			articleManager.removeArticle(reference);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (ArticleDoesNotExist e) {
			LOGGER.info(articleDoesNotExistMessage, reference);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
