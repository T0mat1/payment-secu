package fr.polytech.payment.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.polytech.payment.business.exceptions.UserAlreadyExists;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.business.exceptions.WrongPasswordException;
import fr.polytech.payment.business.managers.UserManager;
import fr.polytech.payment.mapping.dto.UserDTO;

/**
 * Login REST Service
 * @author Thomas Rossi
 * @version 1.1
 */
@RestController
@RequestMapping("/users")
public class UserService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
	
	private static final String DEFAULT_LOGIN_ERROR_MESSAGE = "Invalid username or password.";
	private static final String DEFAULT_SIGN_UP_ERROR_MESSAGE = "This username already exists.";

	
	private UserManager userManager;
	
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	public UserService(UserManager userManager, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userManager = userManager;
		this.encoder = bCryptPasswordEncoder;
	}

	/**
	 * @deprecated Use [ipaddress]:[port]/[version]/login instead
	 * Login method which hash password and return a UserDTO
	 * @param login
	 * @param password
	 * @return a user DTO
	 * @see {@link UserDTO}
	 */
	@Deprecated
	@PostMapping("/login")
	public ResponseEntity<Object> login(@RequestBody UserDTO userData) {
		try {
			return new ResponseEntity<>(userManager.login(userData.getLogin(), userData.getPassword()), HttpStatus.OK);
		} 
		catch (UserNotFoundBusinessException e) {
			LOGGER.info("Cannot connect to the application, cannot find user with given login information.");
			return new ResponseEntity<>(DEFAULT_LOGIN_ERROR_MESSAGE, HttpStatus.FORBIDDEN);
		} catch (WrongPasswordException e) {
			LOGGER.info("Cannot connect to the application, wrong password for the username: {} ", userData.getLogin());
			return new ResponseEntity<>(DEFAULT_LOGIN_ERROR_MESSAGE, HttpStatus.FORBIDDEN);
		}
		
	}
	
	/**
	 * Sign-up method which hash password and return a UserDTO
	 * @param login
	 * @param password
	 * @return a user DTO
	 * @see {@link UserDTO}
	 */
	@PostMapping("/sign-up")
	public ResponseEntity<Object> signUp(@RequestBody UserDTO userData) {
		try {
			return new ResponseEntity<>(userManager.signUp(userData.getLogin(), encoder.encode(userData.getPassword())), HttpStatus.OK);
		}
		catch (UserAlreadyExists e) {
			LOGGER.info("Cannot sign up with this username, it already exists in the database.");
			return new ResponseEntity<>(DEFAULT_SIGN_UP_ERROR_MESSAGE, HttpStatus.FORBIDDEN);
		}
	}

}
