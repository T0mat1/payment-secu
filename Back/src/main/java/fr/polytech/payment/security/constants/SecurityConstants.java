package fr.polytech.payment.security.constants;

public class SecurityConstants {
	
	/**
	 * Private constructor
	 */
	private SecurityConstants() {
		
	}
	
	public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 86_400_000; // 1 day
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";

}
