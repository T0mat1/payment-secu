package fr.polytech.payment.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class EncryptionFactory {
	
	/**
	 * Private constructor, the class should not be instantiate
	 */
	private EncryptionFactory(){
		
	}
	
	public static BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	
    }
}
