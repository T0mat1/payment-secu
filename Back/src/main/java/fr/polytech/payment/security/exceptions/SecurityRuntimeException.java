package fr.polytech.payment.security.exceptions;

public class SecurityRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -514560540868636770L;
	
	public SecurityRuntimeException() {
		
	}
	
	public SecurityRuntimeException(String message) {
		super(message);
	}
	
	public SecurityRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public SecurityRuntimeException(Throwable cause) {
		super(cause);
	}

}
