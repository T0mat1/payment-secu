package fr.polytech.payment.security.exceptions;

public class UserNotFoundException extends Exception {
	private static final long serialVersionUID = -8831107679791166287L;
	
	public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
