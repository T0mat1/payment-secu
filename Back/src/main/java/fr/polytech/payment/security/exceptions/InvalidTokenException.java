package fr.polytech.payment.security.exceptions;

public class InvalidTokenException extends Exception {
	private static final long serialVersionUID = 8354258528894281275L;
	
	public InvalidTokenException() {
    }

    public InvalidTokenException(String message) {
        super(message);
    }

    public InvalidTokenException(String message, Throwable cause) {
        super(message, cause);
    }

}
