package fr.polytech.payment.mapping.mappers.impl;

import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.ObjectMapper;
import fr.polytech.payment.model.User;

/**
 * Mapper to map a user as it is in the model a to user DTO
 * @author Thomas Rossi
 * @version 1.0
 * @see ObjectMapper, User, UserDTO
 */
public class UserToUserDTOMapper implements ObjectMapper<User, UserDTO> {

	/**
	 * Map user to user DTO
	 * Password is set to null
	 */
	@Override
	public UserDTO map(User obj) {
		
		UserDTO dto = new UserDTO();
		dto.setLogin(obj.getLogin());
		dto.setPassword(null);
		return dto;
		
	}

}
