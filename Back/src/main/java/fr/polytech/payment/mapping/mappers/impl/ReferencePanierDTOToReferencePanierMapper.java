package fr.polytech.payment.mapping.mappers.impl;

import fr.polytech.payment.mapping.dto.ReferencePanierDTO;
import fr.polytech.payment.mapping.mappers.ObjectMapper;
import fr.polytech.payment.model.ReferencePanier;

/**
 * Mapper to map a reference of a basket DTO to a reference of a basket as it is in the model
 * @author Thomas Rossi
 * @version 1.0
 * @see {@link ObjectMapper}, {@link ReferencePanierDTO}, {@link ReferencePanier}
 */
public class ReferencePanierDTOToReferencePanierMapper implements ObjectMapper<ReferencePanierDTO, ReferencePanier> {

	/**
	 * Map ReferencePanierDTO to ReferencePanier
	 */
	@Override
	public ReferencePanier map(ReferencePanierDTO obj) {
		ReferencePanier ref = new ReferencePanier();
		ref.setOwner(new UserDTOToUserMapper().map(obj.getOwner()));
		ref.setArticle(new ArticleDTOToArticleMapper().map(obj.getArticle()));
		ref.setQuantity(obj.getQuantity());
		return ref;
	}

}
