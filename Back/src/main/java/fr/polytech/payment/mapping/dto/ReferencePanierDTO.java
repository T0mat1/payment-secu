package fr.polytech.payment.mapping.dto;

import java.io.Serializable;
import java.util.Objects;

public class ReferencePanierDTO implements Serializable {

	private static final long serialVersionUID = 3418320212922094759L;
	
	private UserDTO owner;
	private Integer quantity;
	private ArticleDTO article;
	
	public ReferencePanierDTO() {
		
	}

	public ReferencePanierDTO(UserDTO owner, Integer quantity, ArticleDTO referenceArticle) {
		this.owner = owner;
		this.quantity = quantity;
		this.article = referenceArticle;
	}

	/**
	 * @return the owner
	 */
	public UserDTO getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(UserDTO owner) {
		this.owner = owner;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the referenceArticle
	 */
	public ArticleDTO getArticle() {
		return article;
	}

	/**
	 * @param referenceArticle the referenceArticle to set
	 */
	public void setArticle(ArticleDTO article) {
		this.article = article;
	}
	
	@Override
    public boolean equals(Object object) {
        if (object instanceof ReferencePanierDTO) {
        	return owner.equals(((ReferencePanierDTO) object).getOwner()) && article.equals(((ReferencePanierDTO) object).getArticle());
        } else {
        	return object == this;
        } 
    }
	
	@Override
	public int hashCode() {
		return Objects.hash(owner, article);
	}

}
