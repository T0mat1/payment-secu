package fr.polytech.payment.mapping.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ArticleDTO implements Serializable {

	private static final long serialVersionUID = 2864615452785319039L;

	private String reference;
	private String name;
	private Integer stock;
	private String description;
	private BigDecimal price;

	
	public ArticleDTO() {
	}

	public ArticleDTO(String reference, String name, Integer stock, String description, BigDecimal price) {
		this.reference = reference;
		this.name = name;
		this.stock = stock;
		this.description = description;
		this.price = price;
	}
	
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the stock
	 */
	public Integer getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
    public boolean equals(Object object) {
        return (object instanceof ArticleDTO) 
             ? reference.equals(((ArticleDTO) object).reference) 
             : (object == this);
    }
	
	@Override
	public int hashCode() {
		return reference.hashCode();
	}

}
