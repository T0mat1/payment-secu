package fr.polytech.payment.mapping.mappers.impl;

import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.mapping.mappers.ObjectMapper;
import fr.polytech.payment.model.Article;

public class ArticleDTOToArticleMapper implements ObjectMapper<ArticleDTO, Article> {

	@Override
	public Article map(ArticleDTO obj) {
		Article res = new Article();
		res.setReference(obj.getReference());
		res.setDescription(obj.getDescription());
		res.setName(obj.getName());
		res.setPrice(obj.getPrice());
		res.setStock(obj.getStock());
		return res;
	}

}
