package fr.polytech.payment.mapping.mappers.impl;

import fr.polytech.payment.mapping.dto.ReferencePanierDTO;
import fr.polytech.payment.mapping.mappers.ObjectMapper;
import fr.polytech.payment.model.ReferencePanier;

/**
 * Mapper to map a reference of a basket DTO to a reference of a basket as it is in the model
 * @author Thomas Rossi
 * @version 1.0
 * @see {@link ObjectMapper}, {@link ReferencePanier}, {@link ReferencePanierDTO}
 */
public class ReferencePanierToReferencePanierDTOMapper implements ObjectMapper<ReferencePanier, ReferencePanierDTO> {

	/**
	 * Map ReferencePanier to ReferencePanierDTO
	 */
	@Override
	public ReferencePanierDTO map(ReferencePanier obj) {
		ReferencePanierDTO ref = new ReferencePanierDTO();
		ref.setArticle(new ArticleToArticleDTOMapper().map(obj.getArticle()));
		ref.setOwner(new UserToUserDTOMapper().map(obj.getOwner()));
		ref.setQuantity(obj.getQuantity());
		return ref;
	}

}
