package fr.polytech.payment.mapping.mappers.impl;

import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.ObjectMapper;
import fr.polytech.payment.model.User;

/**
 * Mapper to map a user DTO to a user as it is in the model
 * @author Thomas Rossi
 * @version 1.0
 * @see {@link ObjectMapper}, {@link User}, {@link UserDTO}
 */
public class UserDTOToUserMapper implements ObjectMapper<UserDTO, User> {

	/**
	 * Map user DTO to user
	 */
	@Override
	public User map(UserDTO obj) {
		
		User user = new User();
		user.setLogin(obj.getLogin());
		user.setPassword(obj.getPassword());
		return user;
		
	}

}
