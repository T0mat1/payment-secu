package fr.polytech.payment.mapping.mappers;

/**
 * Functional interface to map an object to another
 * @author Thomas Rossi
 * @version 1.0
 * @param <T> the object to map from
 * @param <R> the object to map to
 * @see FunctionalInterface
 */
@FunctionalInterface
public interface ObjectMapper<T, R> {
	
	/**
	 * Mapping method to map an object to another
	 * @param obj the object of class T to map from
	 * @return the object of class R mapped with the parameter attributes
	 */
	public R map(T obj);

}
