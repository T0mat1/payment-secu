package fr.polytech.payment.mapping.dto;

import java.io.Serializable;

/**
 * Data Transfer Object for mapping model User with REST API data
 * @author Thomas Rossi
 * @version 1.0
 */
public class UserDTO implements Serializable {
	
	private static final long serialVersionUID = 2347493058601787648L;
	
	private String login;
	private String password;
	
	public UserDTO() {
		
	}
	
	public UserDTO(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
    public boolean equals(Object object) {
        return (object instanceof UserDTO) 
             ? login.equals(((UserDTO) object).login) 
             : (object == this);
    }
	
	@Override
	public int hashCode() {
		return login.hashCode();
	}
	
}
