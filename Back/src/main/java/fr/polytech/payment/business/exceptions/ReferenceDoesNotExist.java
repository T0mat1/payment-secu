package fr.polytech.payment.business.exceptions;

/**
 * Exception thrown when a reference is not found
 * @author Thomas Rossi
 * @version 1.0
 */
public class ReferenceDoesNotExist extends Exception {

	private static final long serialVersionUID = -6309169545167310582L;
	
	private static final String DEFAULT_MESSAGE = "The reference does not exist.";

	/**
	 * Constructor which calls the extend one
	 * @param message
	 * @see Exception
	 */
	public ReferenceDoesNotExist(String message) {
		super(message);
	}
	
	/**
	 * Default constructor
	 */
	public ReferenceDoesNotExist() {
		this(DEFAULT_MESSAGE);
	}

}
