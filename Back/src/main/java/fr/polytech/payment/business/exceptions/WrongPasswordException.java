package fr.polytech.payment.business.exceptions;

/**
 * Business exception throwed if the given password is not the same as in the database
 * @author Thomas Rossi
 * @version 1.0
 */
public class WrongPasswordException extends Exception {

	private static final long serialVersionUID = 8531075217024374798L;

	private static final String DEFAULT_MESSAGE = "Wrong password.";

	/**
	 * Constructor which calls the extend one
	 * @param message
	 * @see Exception
	 */
	public WrongPasswordException(String message) {
		super(message);
	}
	
	/**
	 * Default constructor
	 */
	public WrongPasswordException() {
		this(DEFAULT_MESSAGE);
	}

}
