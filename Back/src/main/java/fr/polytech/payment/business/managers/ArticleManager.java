package fr.polytech.payment.business.managers;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.MissingDataException;
import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.mapping.mappers.impl.ArticleDTOToArticleMapper;
import fr.polytech.payment.mapping.mappers.impl.ArticleToArticleDTOMapper;
import fr.polytech.payment.model.Article;
import fr.polytech.payment.repositories.ArticleRepository;

/**
 * Manager class for the articles (Business Layer)
 * @author Thomas Rossi
 * @version 1.0
 */
@Component
public class ArticleManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArticleManager.class);
	
	private static final String NULL_REFERENCE_MESSAGE = "The reference is null";
	
	private ArticleRepository articleRepository;
	private ArticleDTOToArticleMapper dtoToEntity;
	private ArticleToArticleDTOMapper entityToDto;
	
	@Autowired
	public ArticleManager(ArticleRepository articleRepository) {
		this.articleRepository = articleRepository;
		dtoToEntity = new ArticleDTOToArticleMapper();
		entityToDto = new ArticleToArticleDTOMapper();
	}
	
	/**
	 * Get all the articles stored in database
	 * @return a list of all the articles in the database
	 * @see {@link ArticleDTO}
	 */
	public List<ArticleDTO> getAllArticles() {
		List<Article> articles = articleRepository.findAll();
		LOGGER.debug("Retrieve all articles from database and process convertion to DTO.");
		return articles.stream().map(entity -> entityToDto.map(entity)).collect(Collectors.toList());
	}
	
	/**
	 * Get all the articles stored in database which are available
	 * @return a list of all the articles for which the stock is grater than 0
	 * @see {@link ArticleDTO}
	 */
	public List<ArticleDTO> getAllAvailableArticles() {
		List<Article> articles = articleRepository.findAllByStockGreaterThan(Integer.valueOf(0));
		LOGGER.debug("Retrieve all available articles from database and process convertion to DTO.");
		return articles.stream().map(entity -> entityToDto.map(entity)).collect(Collectors.toList());
	}
	
	/**
	 * Get one specific article using its reference
	 * @param reference
	 * @return an instance of {@link ArticleDTO}
	 * @throws ArticleDoesNotExist
	 * @see {@link ArticleDTO}
	 */
	public ArticleDTO getOneArticle(String reference) throws ArticleDoesNotExist {
		LOGGER.debug("Retrieve one article (ref:{}) from database and process convertion to DTO.", reference);
		return entityToDto.map(articleRepository.findByReference(reference).orElseThrow(ArticleDoesNotExist::new));
	}
	
	/**
	 * Add a new article to the database
	 * @param article the data of the article to add
	 * @return the data of the new article
	 * @throws MissingDataException
	 */
	public ArticleDTO addArticle(ArticleDTO article) {
		verifyData(article);
		LOGGER.debug("add one article (ref:{}) to database and process convertion to DTO.", article.getReference());
		articleRepository.save(dtoToEntity.map(article));
		return article;
	}
	
	/**
	 * Remove an article from the database using the reference
	 * @param article the data of the article to remove
	 * @throws ArticleDoesNotExist
	 */
	public void removeArticle(ArticleDTO article) throws ArticleDoesNotExist {
		Article articleToDelete = articleRepository.findByReference(article.getReference()).orElseThrow(ArticleDoesNotExist::new);
		LOGGER.debug("Delete one article (ref:{}) from database.", article.getReference());
		articleRepository.delete(articleToDelete);
	}
	
	/**
	 * Remove an article from the database using the reference
	 * @param reference of the article to remove
	 * @throws ArticleDoesNotExist
	 */
	public void removeArticle(String reference) throws ArticleDoesNotExist {
		Article articleToDelete = articleRepository.findByReference(reference).orElseThrow(ArticleDoesNotExist::new);
		LOGGER.debug("Delete one article (ref:{}) from database.", reference);
		articleRepository.delete(articleToDelete);
	}
	
	/**
	 * 
	 * @param article the new data of the article
	 * @return an instance of {@link ArticleDTO}
	 * @throws ArticleDoesNotExist
	 * @throws MissingDataException 
	 * @see {@link ArticleDTO}
	 */
	public ArticleDTO updateArticle(ArticleDTO article) throws ArticleDoesNotExist {
		Article articleToUpdate = articleRepository.findByReference(article.getReference()).orElseThrow(ArticleDoesNotExist::new);
		verifyData(article);
		LOGGER.debug("Update article ref:{})", article.getReference());
		articleToUpdate.setDescription(article.getDescription());
		articleToUpdate.setName(article.getName());
		articleToUpdate.setPrice(article.getPrice());
		articleToUpdate.setStock(article.getStock());
		return entityToDto.map(articleRepository.save(articleToUpdate));
	}
	
	/**
	 * Verify data of an article
	 * @param article to check
	 * @throws MissingDataException
	 * @see {@link Article}, {@link ArticleDTO}
	 */
	private void verifyData(ArticleDTO article) {
		String reference = article.getReference();
		String name = article.getName();
		LOGGER.debug("Check all mandatory attributes of article (reference, name, stock and price).");
		if (reference == null) {
			throw new NullPointerException(NULL_REFERENCE_MESSAGE);
		}
		if (reference.isBlank() || reference.isEmpty()) {
			throw new MissingDataException(article, "reference");
		}
		if (name == null) {
			throw new MissingDataException(article, "name");
		}
		if (name.isBlank() || name.isEmpty()) {
			throw new MissingDataException(article, "name");
		}
		if (article.getStock() == null) {
			throw new MissingDataException(article, "stock");
		}
		if (article.getPrice() == null) {
			throw new MissingDataException(article, "price");
		}
	}

}
