package fr.polytech.payment.business.exceptions;

/**
 * Business exception throwed if the user already exists in the database.
 * 
 * @author Thomas Rossi
 * @version 1.0
 */
public class ArticleDoesNotExist extends Exception {

	private static final long serialVersionUID = 3383866298440176971L;

	private static final String DEFAULT_MESSAGE = "The article does not exists.";

	/**
	 * Constructor which calls the extend one
	 * 
	 * @param message
	 * @see Exception
	 */
	public ArticleDoesNotExist(String message) {
		super(message);
	}

	/**
	 * Default constructor
	 */
	public ArticleDoesNotExist() {
		this(DEFAULT_MESSAGE);
	}

}
