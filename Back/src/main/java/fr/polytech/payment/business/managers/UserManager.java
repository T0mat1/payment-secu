package fr.polytech.payment.business.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import fr.polytech.payment.business.exceptions.UserAlreadyExists;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.business.exceptions.WrongPasswordException;
import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.impl.UserToUserDTOMapper;
import fr.polytech.payment.model.User;
import fr.polytech.payment.repositories.UserRepository;

/**
 * Manager which handles the business part of the login functionalities
 * @author Thomas Rossi
 * @version 1.0
 */
@Component
public class UserManager {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserManager.class);

	private UserRepository userRepository;
	
	private UserToUserDTOMapper entityToDto;
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	public UserManager(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepository = userRepository;
		entityToDto = new UserToUserDTOMapper();
		encoder = bCryptPasswordEncoder;
	}
	
	/**
	 * Login method of the business layer of the application.
	 * @param login
	 * @param password
	 * @return a {@link UserDTO} entity
	 * @throws UserNotFoundBusinessException
	 */
	public UserDTO login(String login, String password) throws UserNotFoundBusinessException, WrongPasswordException {
		
		LOGGER.debug("Try to log in.");
		User user = userRepository.findByLogin(login).orElseThrow(UserNotFoundBusinessException::new);
		if (encoder.matches(password, user.getPassword())) {
			LOGGER.debug("User {} logged in successfully.", login);
			return entityToDto.map(user);
		}
		else {
			LOGGER.debug("Wrong password for {}.", login);
			throw new WrongPasswordException();
		}
		
	}
	
	/**
	 * Sign up method of the business layer of the application
	 * @param login
	 * @param password
	 * @return a {@link UserDTO}
	 * @throws UserAlreadyExists
	 */
	public UserDTO signUp(String login, String password) throws UserAlreadyExists {
		if (!userRepository.findByLogin(login).isPresent()) { // if there is no user with the given username
			LOGGER.debug("Create new user in database.");
			User user = new User();
			user.setLogin(login);
			user.setPassword(password);
			user = userRepository.save(user);
			return entityToDto.map(user);
		}
		else {
			LOGGER.debug("The username {} is already taken.", login);
			throw new UserAlreadyExists();
		}
		
	}
	
}
