package fr.polytech.payment.business.managers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.ReferenceAlreadyExists;
import fr.polytech.payment.business.exceptions.ReferenceDoesNotExist;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.mapping.dto.ReferencePanierDTO;
import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.impl.ReferencePanierDTOToReferencePanierMapper;
import fr.polytech.payment.mapping.mappers.impl.ReferencePanierToReferencePanierDTOMapper;
import fr.polytech.payment.model.Article;
import fr.polytech.payment.model.ReferencePanier;
import fr.polytech.payment.model.User;
import fr.polytech.payment.repositories.ArticleRepository;
import fr.polytech.payment.repositories.PanierRepository;
import fr.polytech.payment.repositories.UserRepository;

/**
 * Management for payment (Business Layer)
 * 
 * @author Thomas Rossi
 * @version 1.0
 */
@Component
public class PaymentManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentManager.class);
	
	private ArticleRepository articleRepository;
	private PanierRepository panierRepository;
	private UserRepository userRepository;
	
	private ReferencePanierToReferencePanierDTOMapper entityToDto;
	private ReferencePanierDTOToReferencePanierMapper dtoToEntity;

	@Autowired
	public PaymentManager(ArticleRepository articleRepository, PanierRepository panierRepository,
			UserRepository userRepository) {
		this.articleRepository = articleRepository;
		this.panierRepository = panierRepository;
		this.userRepository = userRepository;
		entityToDto = new ReferencePanierToReferencePanierDTOMapper();
		dtoToEntity = new ReferencePanierDTOToReferencePanierMapper();
	}

	/**
	 * Update the basket
	 * @param userData
	 * @return the updated basket
	 * @throws {@link UserNotFoundBusinessException}
	 */
	public List<ReferencePanierDTO> updateBasket(UserDTO userData) throws UserNotFoundBusinessException {
		LOGGER.debug("Update the command of user {}.", userData.getLogin());
		User user = userRepository.findByLogin(userData.getLogin()).orElseThrow(UserNotFoundBusinessException::new);
		return panierRepository.findAllByOwner(user).stream()
				.map(ref -> entityToDto.map(ref))
				.collect(Collectors.toList());
	}

	/**
	 * Empty the command
	 * @param userData the owner of the basket
	 * @throws {@link UserNotFoundBusinessException}
	 */
	public void emptyBasket(UserDTO userData) throws UserNotFoundBusinessException {
		LOGGER.debug("Empty the command of user {}", userData.getLogin());
		User user = userRepository.findByLogin(userData.getLogin()).orElseThrow(UserNotFoundBusinessException::new);
		panierRepository.deleteByOwner(user);
	}

	/**
	 * Check if all the basket is available and return only the available items
	 * @param userData the data in a {@link UserDTO}
	 * @return the list of reference to add
	 * @throws {@link UserNotFoundBusinessException}
	 */
	public List<ReferencePanier> checkBasket(UserDTO userData) throws UserNotFoundBusinessException {
		LOGGER.debug("Filter command.");
		User user = userRepository.findByLogin(userData.getLogin()).orElseThrow(UserNotFoundBusinessException::new);
		List<ReferencePanier> basket = panierRepository.findAllByOwner(user);
		basket = basket.stream().filter(ref -> isArticleAvailable(ref.getArticle(), ref.getQuantity())) //filter only available items
				.collect(Collectors.toList());
		return basket;
	}

	/**
	 * Compute the cost of the command
	 * @param basket a list of {@link ReferencePanier}
	 * @return the cost
	 */
	public BigDecimal processCost(List<ReferencePanier> basket) {
		LOGGER.debug("Process cost of basket.");
		BigDecimal cost = BigDecimal.ZERO;
		for (ReferencePanier ref : basket) {
			cost = cost.add(ref.getArticle().getPrice().multiply(BigDecimal.valueOf(ref.getQuantity())));
		}
		return cost;
	}
	
	/**
	 * Process payment for the given command
	 * @param basket a list of {@link ReferencePanier}
	 * @throws IllegalStateException
	 */
	public void processPayment(List<ReferencePanier> basket) {
		if (basket.stream().allMatch(ref -> isArticleAvailable(ref.getArticle(), ref.getQuantity())) // verify if all articles are available
				&& basket.stream().allMatch(ref -> ref.getOwner().equals(basket.get(0).getOwner()))) { //verify if all the reference have the same owner
			LOGGER.debug("Process payment.");
			for (ReferencePanier reference : basket) {
				Article article = reference.getArticle();
				Integer newStock = article.getStock() - reference.getQuantity();
				article.setStock(newStock); //update stock
				panierRepository.delete(reference);
				articleRepository.save(article);
				
			}
		} else {
			LOGGER.debug("Some articles are not available.");
			throw new IllegalStateException();
		}
	}

	/**
	 * Add a new reference
	 * @param ref the data of the ReferencePanier to add
	 * @throws {@link ArticleDoesNotExist}
	 * @throws {@link UserNotFoundBusinessException}
	 * @throws {@link ReferenceAlreadyExists}
	 */
	public void addReference(ReferencePanierDTO ref) throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceAlreadyExists {
		User owner = userRepository.findByLogin(ref.getOwner().getLogin()).orElseThrow(UserNotFoundBusinessException::new);
		Article article = articleRepository.findByReference(ref.getArticle().getReference()).orElseThrow(ArticleDoesNotExist::new);
		if (panierRepository.findByOwnerAndArticle(owner, article).isEmpty()) {
			LOGGER.debug("Add new reference.");
			ReferencePanier newRef = dtoToEntity.map(ref);
			newRef.setOwner(owner);
			newRef.setArticle(article);
			panierRepository.save(newRef);
		} else {
			LOGGER.debug("This reference already exist, consider updating it instead.");
			throw new ReferenceAlreadyExists();
		}
	}

	/**
	 * Removes safely the reference from the database
	 * @param ref the data of the ReferencePanier DTO
	 * @throws {@link ArticleDoesNotExist}
	 * @throws {@link UserNotFoundBusinessException}
	 * @throws {@link ReferenceDoesNotExist}
	 */
	public void removeReference(ReferencePanierDTO ref) throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		User owner = userRepository.findByLogin(ref.getOwner().getLogin()).orElseThrow(UserNotFoundBusinessException::new);
		Article article = articleRepository.findByReference(ref.getArticle().getReference()).orElseThrow(ArticleDoesNotExist::new);
		ReferencePanier reference = panierRepository.findByOwnerAndArticle(owner, article).orElseThrow(ReferenceDoesNotExist::new);
		LOGGER.debug("This reference does not exist.");
		panierRepository.delete(reference);
	}
	
	/**
	 * Update the reference from the database
	 * @param ref the data of the ReferencePanier DTO
	 * @throws {@link ArticleDoesNotExist}
	 * @throws {@link UserNotFoundBusinessException}
	 * @throws {@link ReferenceDoesNotExist}
	 */
	public void updateReference(ReferencePanierDTO ref) throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		User owner = userRepository.findByLogin(ref.getOwner().getLogin()).orElseThrow(UserNotFoundBusinessException::new);
		Article article = articleRepository.findByReference(ref.getArticle().getReference()).orElseThrow(ArticleDoesNotExist::new);
		Optional<ReferencePanier> refDB = panierRepository.findByOwnerAndArticle(owner, article);
		if (refDB.isEmpty()) {
			LOGGER.debug("This reference does not exist.");
			throw new ReferenceDoesNotExist();
		}
		LOGGER.debug("Update the reference.");
		ReferencePanier newRef = refDB.get();
		newRef.setQuantity(ref.getQuantity());
		panierRepository.save(newRef);
	}

	/**
	 * Method to know if an article is available
	 * @param article the {@link Article}
	 * @param quantity the quantity to buy
	 * @return <i>true</i> if the article is available for the given quantity, <i>false</i> otherwise
	 */
	public boolean isArticleAvailable(Article article, Integer quantity) {
		return article.getStock() > quantity;
	}

}
