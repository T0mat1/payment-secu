package fr.polytech.payment.business.exceptions;

/**
 * Business exception throwed if the user already exists in the database.
 * @author Thomas Rossi
 * @version 1.0
 */
public class UserAlreadyExists extends Exception {
	
	private static final long serialVersionUID = 361250909047261368L;
	
	private static final String DEFAULT_MESSAGE = "User username already exists.";

	/**
	 * Constructor which calls the extend one
	 * @param message
	 * @see Exception
	 */
	public UserAlreadyExists(String message) {
		super(message);
	}
	
	/**
	 * Default constructor
	 */
	public UserAlreadyExists() {
		this(DEFAULT_MESSAGE);
	}

}
