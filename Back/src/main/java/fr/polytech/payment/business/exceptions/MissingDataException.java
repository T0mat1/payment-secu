package fr.polytech.payment.business.exceptions;

public class MissingDataException extends RuntimeException {

	private static final long serialVersionUID = -3268229840264586694L;
	
	private static final String DEFAULT_MESSAGE = "There are missing data to perform an operation.";

	/**
	 * 
	 * @param object the object continaing data
	 * @param missingValue the missing field
	 */
	public MissingDataException(Object object, String missingValue) {
		this(buildMessage(object, missingValue));
	}
	
	/**
	 * Constructor which calls the extend one
	 * 
	 * @param message
	 * @see Exception
	 */
	public MissingDataException(String message) {
		super(message);
	}

	/**
	 * Default constructor
	 */
	public MissingDataException() {
		this(DEFAULT_MESSAGE);
	}
	
	/**
	 * Build a custom message to indicate which attribute is missing
	 * @param object the object continaing data
	 * @param missingValue the missing field
	 * @return a string containing the message
	 */
	private static String buildMessage(Object object, String missingValue) {
		StringBuilder message = new StringBuilder(DEFAULT_MESSAGE);
		message.append("The field: " + missingValue);
		message.append(" of the class: " + object.getClass().getSimpleName());
		message.append("is missing");
		return message.toString();
	}

}
