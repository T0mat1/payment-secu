package fr.polytech.payment.business.services;

import static java.util.Collections.emptyList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.polytech.payment.repositories.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    
	@Autowired
	private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) {
        fr.polytech.payment.model.User applicationUser = userRepository.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new User(applicationUser.getLogin(), applicationUser.getPassword(), emptyList());
    }
}