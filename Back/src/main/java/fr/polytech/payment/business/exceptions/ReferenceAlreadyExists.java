package fr.polytech.payment.business.exceptions;

/**
 * Exception thrown when a reference already exists
 * @author Thomas Rossi
 * @version 1.0
 */
public class ReferenceAlreadyExists extends Exception {

	private static final long serialVersionUID = -8387013392616023199L;
	
	private static final String DEFAULT_MESSAGE = "This reference already exists.";

	/**
	 * Constructor which calls the extend one
	 * @param message
	 * @see Exception
	 */
	public ReferenceAlreadyExists(String message) {
		super(message);
	}
	
	/**
	 * Default constructor
	 */
	public ReferenceAlreadyExists() {
		this(DEFAULT_MESSAGE);
	}

}
