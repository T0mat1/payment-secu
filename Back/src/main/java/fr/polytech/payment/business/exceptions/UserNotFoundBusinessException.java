package fr.polytech.payment.business.exceptions;

/**
 * Business exception throwed if a user can't be found in the database
 * @author Thomas Rossi
 * @version 1.0
 */
public class UserNotFoundBusinessException extends Exception {

	private static final long serialVersionUID = -953095430127051654L;
	
	private static final String DEFAULT_MESSAGE = "Cannot find user in the database with the given parameters";

	/**
	 * Constructor which calls the extend one
	 * @param message
	 * @see Exception
	 */
	public UserNotFoundBusinessException(String message) {
		super(message);
	}
	
	/**
	 * Default constructor
	 */
	public UserNotFoundBusinessException() {
		this(DEFAULT_MESSAGE);
	}

}
