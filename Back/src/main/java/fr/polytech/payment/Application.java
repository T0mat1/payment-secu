package fr.polytech.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fr.polytech.payment.security.EncryptionFactory;


@SpringBootApplication
public class Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
	
	private static final String VERSION = "/v1";
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return EncryptionFactory.bCryptPasswordEncoder();
	
    }
	
	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", VERSION);
		SpringApplication paymentApp = new SpringApplication(Application.class);
		Environment env = paymentApp.run(args).getEnvironment();
		LOGGER.info("URL d'accès local : http://localhost:{}", env.getProperty("server.port"));
	}

}
