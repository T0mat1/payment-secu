package fr.polytech.payment.mapping.mappers.impl;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.model.Article;

@RunWith(MockitoJUnitRunner.class)
class ArticleDTOToArticleMapperTest {
	
	private static final String REFERENCE = "REF000";
	private static final String NAME = "Product 1";
	private static final Integer STOCK = Integer.valueOf(5);
	private static final String DESCRIPTION = "Description of product 1.";
	private static final BigDecimal PRICE = new BigDecimal(15.99);
	private static final ArticleDTO EXPECTED_RESULT = new ArticleDTO(REFERENCE, NAME, STOCK, DESCRIPTION, PRICE);
	
	private ArticleToArticleDTOMapper mapper = new ArticleToArticleDTOMapper();

	@Test
	public void mapOK() {
		Article articleToMap = new Article();
		ReflectionTestUtils.setField(articleToMap, "idArticle", UUID.randomUUID());
		articleToMap.setName(NAME);
		articleToMap.setDescription(DESCRIPTION);
		articleToMap.setReference(REFERENCE);
		articleToMap.setStock(STOCK);
		articleToMap.setPrice(PRICE);
		Assertions.assertEquals(EXPECTED_RESULT, mapper.map(articleToMap));
	}

}
