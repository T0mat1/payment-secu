package fr.polytech.payment.mapping.mappers.impl;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.model.User;

public class UserToUserDTOMapperTest {
	
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final UserDTO EXPECTED_RESULT = new UserDTO(USERNAME, null);
	
	@Test
	void mapOK() {
		UserToUserDTOMapper mapper = new UserToUserDTOMapper();
		User usr = new User();
		usr.setLogin(USERNAME);
		usr.setPassword(PASSWORD);
		Assert.assertEquals(EXPECTED_RESULT, mapper.map(usr));
	}

}
