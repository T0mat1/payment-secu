package fr.polytech.payment.mapping.mappers.impl;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.model.Article;

@RunWith(MockitoJUnitRunner.class)
class ArticleToArticleDTOMapperTest {
	
	private static final String REFERENCE = "REF000";
	private static final String NAME = "Product 1";
	private static final Integer STOCK = Integer.valueOf(5);
	private static final String DESCRIPTION = "Description of product 1.";
	private static final BigDecimal PRICE = new BigDecimal(15.99);
	private static final Article EXPECTED_RESULT = new Article(REFERENCE, NAME, STOCK, DESCRIPTION, PRICE);

	private ArticleDTOToArticleMapper mapper = new ArticleDTOToArticleMapper();
	
	@Test
	public void test() {
		ArticleDTO articleToMap = new ArticleDTO(REFERENCE, NAME, STOCK, DESCRIPTION, PRICE);
		Assertions.assertEquals(EXPECTED_RESULT, mapper.map(articleToMap));
	}

}
