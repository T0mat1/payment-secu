package fr.polytech.payment.mapping.mappers.impl;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.model.User;

public class UserDTOToUserMapperTest {

	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final User EXPECTED_RESULT = new User(USERNAME, PASSWORD);
	
	@Test
	void mapOK() {
		UserDTOToUserMapper mapper = new UserDTOToUserMapper();
		UserDTO usr = new UserDTO(USERNAME, PASSWORD);
		Assert.assertEquals(EXPECTED_RESULT, mapper.map(usr));
	}

}
