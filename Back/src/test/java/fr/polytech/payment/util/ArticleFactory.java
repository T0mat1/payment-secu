package fr.polytech.payment.util;

import java.math.BigDecimal;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.model.Article;

/**
 * Article and ArticleDTO factory for test purpose
 * @author Thomas Rossi
 * @version 1.0
 */
public class ArticleFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArticleFactory.class);
	
	/*
	 * Private constructor, this class must not be instantiate
	 */
	private ArticleFactory() {
		
	}

	/**
	 * Create new article with uuid to mock database entity
	 * 
	 * @param reference
	 * @param name
	 * @param description
	 * @param stock
	 * @param price
	 * @return the article
	 * @see {@link Article}
	 */
	public static Article createNewArticleAndId(String reference, String name, String description, Integer stock,
			BigDecimal price) {
		Article article = new Article(reference, name, stock, description, price);
		ReflectionTestUtils.setField(article, "idArticle", UUID.randomUUID());
		LOGGER.debug("Create new article : UUID={}, reference={}, name={}, description={}, price={}, stock={}",
				article.getId(), article.getReference(), article.getName(), article.getDescription(),
				article.getPrice(), article.getStock());
		return article;
	}

	/**
	 * Create a new random article for tests
	 * 
	 * @return a {@link Article} with random values
	 * @see {@link TestUtils}
	 */
	public static Article createNewRandomArticle() {
		Article article = new Article();
		ReflectionTestUtils.setField(article, "idArticle", UUID.randomUUID());
		article.setName(TestUtils.randomString(10));
		article.setDescription(TestUtils.randomString(20));
		article.setReference(TestUtils.randomString(8));
		article.setPrice(TestUtils.randomBigDecimal());
		article.setStock(TestUtils.randomInteger(Integer.MAX_VALUE));
		LOGGER.debug("Create new random article : UUID={}, reference={}, name={}, description={}, price={}, stock={}",
				article.getId(), article.getReference(), article.getName(), article.getDescription(),
				article.getPrice(), article.getStock());
		return article;
	}

	/**
	 * Create a new random article dto for tests
	 * 
	 * @return a {@link ArticleDTO} with random values
	 * @see {@link TestUtils}
	 */
	public static ArticleDTO createNewRandomArticleDto() {
		ArticleDTO article = new ArticleDTO();
		article.setName(TestUtils.randomString(10));
		article.setDescription(TestUtils.randomString(20));
		article.setReference(TestUtils.randomString(8));
		article.setPrice(TestUtils.randomBigDecimal());
		article.setStock(TestUtils.randomInteger());
		LOGGER.debug("Create new random article : reference={}, name={}, description={}, price={}, stock={}",
				article.getReference(), article.getName(), article.getDescription(), article.getPrice(),
				article.getStock());
		return article;
	}

}
