package fr.polytech.payment.util;

import java.math.BigDecimal;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for tests
 * @author Thomas Rossi
 * @version 1.0
 */
public class TestUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestUtils.class);
	private static final long SEED = 22042020l;
	private static final Random RNG = new Random(SEED);
	
	private static final int CHAR_0 = 48;
	private static final int CHAR_z = 122;
	
	/*
	 * Private constructor, this class must not be instantiate
	 */
	private TestUtils() {
		
	}
	
	/**
	 * Create a random string
	 * @param length of the String
	 * @return a random {@link String}
	 */
	public static String randomString(int length) {
		byte[] array = new byte[length];
	    RNG.nextBytes(array);
	    String result = RNG.ints(CHAR_0, CHAR_z + 1)
	    	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	    	      .limit(length)
	    	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	    	      .toString();
	    LOGGER.trace("Generate new random string: {}.", result);
	    return result;
	}
	
	/**
	 * Create a random integer
	 * @return a random {@link Integer}
	 */
	public static Integer randomInteger() {
		int value = RNG.nextInt();
		Integer result = Integer.valueOf(value);
	    LOGGER.trace("Generate new random integer: {}.", result);
		return result;
	}
	
	/**
	 * Create a random integer between 0 and the bound
	 * @param bound
	 * @return a random {@link Integer}
	 */
	public static Integer randomInteger(int bound) {
		int value = RNG.nextInt(bound);
		Integer result = Integer.valueOf(value);
	    LOGGER.trace("Generate new random integer: {}.", result);
		return result;
	}
	
	/**
	 * Create a random BigDecimal
	 * @return a random {@link BigDecimal}
	 */
	public static BigDecimal randomBigDecimal() {
		double value = RNG.nextDouble();
		BigDecimal result = BigDecimal.valueOf(value);
	    LOGGER.trace("Generate new random BigDecimal: {}.", result);
		return result;
	}

}
