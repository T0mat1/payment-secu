package fr.polytech.payment.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fr.polytech.payment.business.exceptions.UserAlreadyExists;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.business.exceptions.WrongPasswordException;
import fr.polytech.payment.business.managers.UserManager;
import fr.polytech.payment.mapping.dto.UserDTO;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserServiceTest {
	
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final ResponseEntity<Object> EXPECTED_RESULT_OK = new ResponseEntity<>(new UserDTO(USERNAME, null), HttpStatus.OK);
	
	@InjectMocks
	@Autowired
	private UserService service;
	
	@Mock
	private UserManager manager;
	
	@Mock
	private BCryptPasswordEncoder encoder;
	
	
	@BeforeAll
	public void onSetUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void loginOK() throws UserNotFoundBusinessException, WrongPasswordException {
		Mockito.doReturn(new UserDTO(USERNAME, null)).when(manager).login(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
		Assertions.assertEquals(EXPECTED_RESULT_OK, service.login(new UserDTO(USERNAME, PASSWORD)));	
	}
	
	@Test
	public void loginWrongPasswordKO() throws UserNotFoundBusinessException, WrongPasswordException {
		Mockito.doThrow(new WrongPasswordException()).when(manager).login(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.FORBIDDEN, service.login(new UserDTO(USERNAME, PASSWORD)).getStatusCode());
	}
	
	@Test
	public void loginWrongUsernameKO() throws UserNotFoundBusinessException, WrongPasswordException {
		Mockito.doThrow(new UserNotFoundBusinessException()).when(manager).login(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.FORBIDDEN, service.login(new UserDTO(USERNAME, PASSWORD)).getStatusCode());
	}
	
	@Test
	public void signUpOK() throws UserAlreadyExists {
		Mockito.doReturn(new UserDTO(USERNAME, null)).when(manager).signUp(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
		Mockito.doReturn(PASSWORD).when(encoder).encode(ArgumentMatchers.anyString());
		Assertions.assertEquals(EXPECTED_RESULT_OK, service.signUp(new UserDTO(USERNAME, PASSWORD)));
	}
	
	@Test
	public void signUpKO() throws UserAlreadyExists {
		Mockito.doThrow(new UserAlreadyExists()).when(manager).signUp(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
		Mockito.doReturn(PASSWORD).when(encoder).encode(ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.FORBIDDEN, service.signUp(new UserDTO(USERNAME, PASSWORD)).getStatusCode());
	}
}