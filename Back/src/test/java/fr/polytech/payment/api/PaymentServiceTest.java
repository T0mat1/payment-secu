package fr.polytech.payment.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.ReferenceAlreadyExists;
import fr.polytech.payment.business.exceptions.ReferenceDoesNotExist;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.business.managers.PaymentManager;
import fr.polytech.payment.mapping.dto.ReferencePanierDTO;
import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.impl.ReferencePanierToReferencePanierDTOMapper;
import fr.polytech.payment.mapping.mappers.impl.UserDTOToUserMapper;
import fr.polytech.payment.model.ReferencePanier;
import fr.polytech.payment.util.ArticleFactory;
import fr.polytech.payment.util.TestUtils;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
class PaymentServiceTest {
	
	private static final UserDTO USER = new UserDTO("username", "password");
	private static final Integer MAX_QUANTITY = Integer.valueOf(10);
	private static final Integer MAX_BASKET_LENGTH = Integer.valueOf(10);
	
	private List<ReferencePanierDTO> randomBasket;
	
	@InjectMocks
	@Autowired
	private PaymentService service;
	
	@Mock
	private PaymentManager paymentManager;

	@BeforeAll
	void onSetUp() {
		MockitoAnnotations.initMocks(this);
		randomBasket = new ArrayList<>();
	}
	
	@BeforeEach
	void setUp() {
		for (int i=0; i<TestUtils.randomInteger(MAX_BASKET_LENGTH)+1; i++) {
			ReferencePanierDTO reference = new ReferencePanierDTO();
			reference.setArticle(ArticleFactory.createNewRandomArticleDto());
			reference.setOwner(USER);
			reference.setQuantity(TestUtils.randomInteger(MAX_QUANTITY-1)+1); // exclude zero
			randomBasket.add(reference);
		}
	}
	
	@AfterEach
	void cleanUpEach() {
		randomBasket.clear();
	}

	@Test
	void getAllBasketReferencesOK() throws UserNotFoundBusinessException {
		Mockito.doReturn(randomBasket).when(paymentManager).updateBasket(ArgumentMatchers.any());
		Assertions.assertEquals(new ResponseEntity<>(randomBasket, HttpStatus.OK), service. getAllBasketReferences(USER));
	}
	
	@Test
	void getAllBasketReferencesKO() throws UserNotFoundBusinessException {
		Mockito.doThrow(new UserNotFoundBusinessException()).when(paymentManager).updateBasket(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service. getAllBasketReferences(USER).getStatusCode());
	}

	@Test
	void emptyBasketOK() throws UserNotFoundBusinessException {
		Mockito.doNothing().when(paymentManager).emptyBasket(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.OK, service.emptyBasket(USER).getStatusCode());
	}
	
	@Test
	void emptyBasketKO() throws UserNotFoundBusinessException {
		Mockito.doThrow(new UserNotFoundBusinessException()).when(paymentManager).emptyBasket(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.emptyBasket(USER).getStatusCode());
	}

	@Test
	void getCostOK() {
		BigDecimal cost = TestUtils.randomBigDecimal();
		Mockito.doReturn(cost).when(paymentManager).processCost(ArgumentMatchers.anyList());
		Assertions.assertEquals(new ResponseEntity<>(cost, HttpStatus.OK), service.getCost(USER));
	}

	@Test
	void confirmPaymentOK() {
		Mockito.doNothing().when(paymentManager).processPayment(ArgumentMatchers.anyList());
		Assertions.assertEquals(HttpStatus.OK, service.confirmPayment(USER).getStatusCode());
	}
	
	@Test
	void confirmPaymentIllegalStateKO() {
		Mockito.doThrow(new IllegalStateException()).when(paymentManager).processPayment(ArgumentMatchers.anyList());
		Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, service.confirmPayment(USER).getStatusCode());
	}

	@Test
	void addReferenceOK() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceAlreadyExists {
		Mockito.doReturn(Boolean.TRUE).when(paymentManager).isArticleAvailable(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.doNothing().when(paymentManager).addReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.OK, service.addReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void addReferenceUserDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceAlreadyExists {
		Mockito.doReturn(Boolean.TRUE).when(paymentManager).isArticleAvailable(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.doThrow(new UserNotFoundBusinessException()).when(paymentManager).addReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.addReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void addReferenceArticleDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceAlreadyExists {
		Mockito.doReturn(Boolean.TRUE).when(paymentManager).isArticleAvailable(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.doThrow(new ArticleDoesNotExist()).when(paymentManager).addReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.addReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void addReferenceAlreadyExistsKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceAlreadyExists {
		Mockito.doReturn(Boolean.TRUE).when(paymentManager).isArticleAvailable(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.doThrow(new ReferenceAlreadyExists()).when(paymentManager).addReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.addReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void addReferenceArticleIsNotAvailableKO() {
		Mockito.doReturn(Boolean.FALSE).when(paymentManager).isArticleAvailable(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.addReference(randomBasket.get(0)).getStatusCode());
	}

	@Test
	void updateReferenceOK() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doNothing().when(paymentManager).updateReference(ArgumentMatchers.any());
		Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK), service.updateReference(randomBasket.get(0)));
	}
	
	@Test
	void updateReferenceArticleDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doThrow(new ArticleDoesNotExist()).when(paymentManager).updateReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.updateReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void updateReferenceUserDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doThrow(new UserNotFoundBusinessException()).when(paymentManager).updateReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.updateReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void updateReferenceDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doThrow(new ReferenceDoesNotExist()).when(paymentManager).updateReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.updateReference(randomBasket.get(0)).getStatusCode());
	}

	@Test
	void deleteReferenceOK() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doNothing().when(paymentManager).removeReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.OK, service.deleteReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void deleteReferenceArticleDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doThrow(new ArticleDoesNotExist()).when(paymentManager).removeReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.deleteReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void deleteReferenceUserDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doThrow(new UserNotFoundBusinessException()).when(paymentManager).removeReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.deleteReference(randomBasket.get(0)).getStatusCode());
	}
	
	@Test
	void deleteReferenceDoesNotExistKO() throws ArticleDoesNotExist, UserNotFoundBusinessException, ReferenceDoesNotExist {
		Mockito.doThrow(new ReferenceDoesNotExist()).when(paymentManager).removeReference(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.deleteReference(randomBasket.get(0)).getStatusCode());
	}

	@Test
	void getAllAvailableItemsOK() throws UserNotFoundBusinessException {
		List<ReferencePanier> entities = new ArrayList<>();
		for (int i=0; i<TestUtils.randomInteger(MAX_BASKET_LENGTH)+1; i++) {
			ReferencePanier ref = new ReferencePanier();
			ref.setArticle(ArticleFactory.createNewRandomArticle());
			ref.setOwner(new UserDTOToUserMapper().map(USER));
			ref.setQuantity(TestUtils.randomInteger(MAX_QUANTITY-1)+1); // exclude zero
			entities.add(ref);
		}
		Mockito.doReturn(entities).when(paymentManager).checkBasket(ArgumentMatchers.any());
		List<ReferencePanierDTO> expected = entities.stream().map(ref -> new ReferencePanierToReferencePanierDTOMapper().map(ref)).collect(Collectors.toList());
		Assertions.assertEquals(new ResponseEntity<>(expected, HttpStatus.OK), service.getAllAvailableItems(USER));
	}
	
	@Test
	void getAllAvailableItemsKO() throws UserNotFoundBusinessException {
		Mockito.doThrow(new UserNotFoundBusinessException()).when(paymentManager).checkBasket(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.getAllAvailableItems(USER).getStatusCode());
	}

}
