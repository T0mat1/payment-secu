package fr.polytech.payment.api;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.MissingDataException;
import fr.polytech.payment.business.managers.ArticleManager;
import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.util.ArticleFactory;
import fr.polytech.payment.util.TestUtils;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
class ArticleServiceTest {
	
	private static final String REFERENCE = "REF0001";
	private static final int MAX_QUANTITY_OF_ARTICLES = 30;
	
	@InjectMocks
	@Autowired
	private ArticleService service;
	
	@Mock
	private ArticleManager articleManager;

	@BeforeAll
	void onSetUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void getAllArticlesOK() {
		ArrayList<ArticleDTO> articles = new ArrayList<>();
		for (int i=0; i<TestUtils.randomInteger(MAX_QUANTITY_OF_ARTICLES); i++) {
			articles.add(ArticleFactory.createNewRandomArticleDto());
		}
		Mockito.doReturn(articles).when(articleManager).getAllArticles();
		Assertions.assertEquals(HttpStatus.OK, service.getAllArticles().getStatusCode());
	}

	@Test
	void getAllAvailableArticlesOK() {
		ArrayList<ArticleDTO> articles = new ArrayList<>();
		for (int i=0; i<TestUtils.randomInteger(MAX_QUANTITY_OF_ARTICLES); i++) {
			ArticleDTO article = ArticleFactory.createNewRandomArticleDto();
			article.setStock(Integer.valueOf(0));
			articles.add(article);
		}
		Mockito.doReturn(articles).when(articleManager).getAllArticles();
		Assertions.assertEquals(HttpStatus.OK, service.getAllArticles().getStatusCode());
	}

	@Test
	void getOneArticleOK() throws ArticleDoesNotExist {
		Mockito.doReturn(ArticleFactory.createNewRandomArticleDto()).when(articleManager).getOneArticle(ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.OK, service.getOneArticle(REFERENCE).getStatusCode());
	}
	
	@Test
	void getOneArticleKO() throws ArticleDoesNotExist {
		Mockito.doThrow(new ArticleDoesNotExist()).when(articleManager).getOneArticle(ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.NOT_FOUND, service.getOneArticle(REFERENCE).getStatusCode());
	}

	@Test
	void createNewArticleOK() {
		Mockito.doReturn(ArticleFactory.createNewRandomArticleDto()).when(articleManager).addArticle(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.CREATED, service.createNewArticle(ArticleFactory.createNewRandomArticleDto()).getStatusCode());
	}
	
	@Test
	void createNewArticleKO() {
		Mockito.doThrow(new MissingDataException()).when(articleManager).addArticle(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.createNewArticle(ArticleFactory.createNewRandomArticleDto()).getStatusCode());
	}

	@Test
	void updateArticleOK() throws MissingDataException, ArticleDoesNotExist {
		Mockito.doReturn(ArticleFactory.createNewRandomArticleDto()).when(articleManager).updateArticle(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.OK, service.updateArticle(ArticleFactory.createNewRandomArticleDto()).getStatusCode());
	}
	
	@Test
	void updateArticleMissingDataKO() throws MissingDataException, ArticleDoesNotExist {
		Mockito.doThrow(new MissingDataException()).when(articleManager).updateArticle(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.updateArticle(ArticleFactory.createNewRandomArticleDto()).getStatusCode());
	}
	
	@Test
	void updateArticleArticleDoesNotExistsKO() throws MissingDataException, ArticleDoesNotExist {
		Mockito.doThrow(new MissingDataException()).when(articleManager).updateArticle(ArgumentMatchers.any());
		Assertions.assertEquals(HttpStatus.BAD_REQUEST, service.updateArticle(ArticleFactory.createNewRandomArticleDto()).getStatusCode());
	}

	@Test
	void deleteArticleOK() throws ArticleDoesNotExist {
		Mockito.doNothing().when(articleManager).removeArticle(ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.OK, service.deleteArticle(REFERENCE).getStatusCode());
	}
	
	@Test
	void deleteArticleKO() throws ArticleDoesNotExist {
		Mockito.doThrow(new ArticleDoesNotExist()).when(articleManager).removeArticle(ArgumentMatchers.anyString());
		Assertions.assertEquals(HttpStatus.NOT_FOUND, service.deleteArticle(REFERENCE).getStatusCode());
	}

}
