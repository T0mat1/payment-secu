package fr.polytech.payment.business.managers;

import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.MissingDataException;
import fr.polytech.payment.mapping.dto.ArticleDTO;
import fr.polytech.payment.model.Article;
import fr.polytech.payment.repositories.ArticleRepository;
import fr.polytech.payment.util.ArticleFactory;
import fr.polytech.payment.util.TestUtils;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
class ArticleManagerTest {

	private static final String REFERENCE = "REF001";
	private static final String NAME = "Product 1";
	private static final String DESCRIPTION = "Description";
	private static final Integer STOCK = Integer.valueOf(12);
	private static final BigDecimal PRICE = BigDecimal.valueOf(8.99);
	private static final int MAX_QUANTITY_OF_ARTICLES = 30;

	@Autowired
	@InjectMocks
	private ArticleManager manager;

	@Mock
	private ArticleRepository articleRepository;

	@BeforeAll
	public void onSetUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllArticlesEmptyListOK() {
		Mockito.doReturn(new ArrayList<>()).when(articleRepository).findAll();
		Assertions.assertEquals(0, manager.getAllArticles().size());
	}

	@Test
	public void getAllAvailableArticlesOK() {
		List<Article> articles = new ArrayList<>();
		for (int i = 0; i < TestUtils.randomInteger(MAX_QUANTITY_OF_ARTICLES); i++) {
			Article article = ArticleFactory.createNewRandomArticle();
			if (article.getStock() == 0) {
				article.setStock(Integer.valueOf(STOCK));
			}
			articles.add(article);
		}
		Mockito.doReturn(articles).when(articleRepository).findAllByStockGreaterThan(0);
		List<ArticleDTO> result = manager.getAllAvailableArticles();
		for (ArticleDTO dto : result) {
			Assertions.assertNotEquals(0, dto.getStock());
		}
	}

	@Test
	public void getAllAvailableArticlesEmptyListOK() {
		Mockito.doReturn(new ArrayList<>()).when(articleRepository).findAllByStockGreaterThan(0);
		Assertions.assertEquals(0, manager.getAllArticles().size());
	}

	@Test
	public void getOneArticleKO() {
		Mockito.doReturn(Optional.empty()).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Assertions.assertThrows(ArticleDoesNotExist.class, () -> manager.getOneArticle(REFERENCE));
	}

	@Test
	public void updateArticleOK() {
		Mockito.doReturn(
				Optional.of(new Article(REFERENCE, "old_name", 0, "old_description", BigDecimal.valueOf(8.50))))
				.when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(ArticleFactory.createNewArticleAndId(REFERENCE, NAME, DESCRIPTION, STOCK, PRICE))
				.when(articleRepository).save(ArgumentMatchers.any());
		ArticleDTO dto = new ArticleDTO(REFERENCE, NAME, STOCK, DESCRIPTION, PRICE);
		ArticleDTO result;
		try {
			result = manager.updateArticle(dto);
			Assertions.assertEquals(REFERENCE, result.getReference());
			Assertions.assertEquals(NAME, result.getName());
			Assertions.assertEquals(STOCK, result.getStock());
			Assertions.assertEquals(DESCRIPTION, result.getDescription());
			Assertions.assertEquals(PRICE, result.getPrice());
		} catch (ArticleDoesNotExist | MissingDataException e) {
			fail("No exception must be caught here.", e);
		}
	}

	@Test
	public void updateArticleKO() {
		Mockito.doReturn(Optional.empty()).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Assertions.assertThrows(ArticleDoesNotExist.class, () -> manager.updateArticle(new ArticleDTO()));
	}

	@Test
	public void verifyDataOK() {
		try {
		ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData",
				ArticleFactory.createNewRandomArticleDto());
		} catch (Exception e) {
			fail("No exception must be caught here.", e);
		}
	}

	@Test
	public void verifyDataReferenceNullKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setReference(null);
		Assertions.assertTrue(dto.getReference() == null);
		Assertions.assertThrows(NullPointerException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDataReferenceBlankKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setReference("   ");
		Assertions.assertTrue(dto.getReference().isBlank());
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDataReferenceEmptyKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setReference("");
		Assertions.assertTrue(dto.getReference().isEmpty());
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDataNameNullKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setName(null);
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDataNameBlankKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setName("   ");
		Assertions.assertTrue(dto.getName().isBlank());
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDataNameEmptyKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setName("");
		Assertions.assertTrue(dto.getName().isEmpty());
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDataStockNullKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setStock(null);
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));
	}

	@Test
	public void verifyDatapPriceNullKO() {
		ArticleDTO dto = ArticleFactory.createNewRandomArticleDto();
		dto.setPrice(null);
		Assertions.assertThrows(MissingDataException.class,
				() -> ReflectionTestUtils.invokeMethod(manager, ArticleManager.class, "verifyData", dto));

	}
}
