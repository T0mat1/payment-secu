package fr.polytech.payment.business.managers;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import fr.polytech.payment.business.exceptions.UserAlreadyExists;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.business.exceptions.WrongPasswordException;
import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.model.User;
import fr.polytech.payment.repositories.UserRepository;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserManagerTest {
	
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final UserDTO EXPECTED_RESULT_OK = new UserDTO(USERNAME, null);
	
	private User databaseUser;
	
	@Mock
	private UserRepository userRepository = Mockito.mock(UserRepository.class);
	
	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);
	
	@InjectMocks
	@Autowired
	private UserManager manager;
	
	
	@BeforeAll
	public void onSetUp() {
		MockitoAnnotations.initMocks(this);
		databaseUser = new User();
		ReflectionTestUtils.setField(databaseUser, "idUser", UUID.randomUUID());
		databaseUser.setLogin(USERNAME);
		databaseUser.setPassword(PASSWORD);
			
	}
	
	@Test
	public void loginOK() {
		Mockito.when(userRepository.findByLogin(ArgumentMatchers.anyString())).thenReturn(Optional.of(databaseUser));
		Mockito.when(bCryptPasswordEncoder.matches(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(Boolean.TRUE);
		try {
			Assert.assertEquals(EXPECTED_RESULT_OK, manager.login(USERNAME, PASSWORD));
			
		} catch (UserNotFoundBusinessException | WrongPasswordException e) {
			fail("No exception must be caught here.", e);
		}		
	}
	
	@Test
	public void loginWrongPasswordKO() throws WrongPasswordException {
		Mockito.when(userRepository.findByLogin(ArgumentMatchers.anyString())).thenReturn(Optional.of(databaseUser));
		Mockito.when(bCryptPasswordEncoder.matches(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(Boolean.FALSE);
		Assertions.assertThrows(WrongPasswordException.class, () -> manager.login(USERNAME, PASSWORD));
	}
	
	@Test
	public void loginWrongUsernameKO() {
		Mockito.when(userRepository.findByLogin(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
		Mockito.when(bCryptPasswordEncoder.matches(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(Boolean.TRUE);
		Assertions.assertThrows(UserNotFoundBusinessException.class, () -> manager.login(USERNAME, PASSWORD));
	}
	
	@Test
	public void signUpOK() {
		Mockito.when(userRepository.findByLogin(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
		Mockito.when(userRepository.save(ArgumentMatchers.any())).thenReturn(databaseUser);
		try {
			Assert.assertEquals(EXPECTED_RESULT_OK, manager.signUp(USERNAME, PASSWORD));
		} catch (UserAlreadyExists e) {
			fail("No exception must be caught here.", e);
		}
	}
	
	@Test
	public void signUpKO() {
		Mockito.when(userRepository.findByLogin(ArgumentMatchers.anyString())).thenReturn(Optional.of(databaseUser));
		Assertions.assertThrows(UserAlreadyExists.class, () -> manager.signUp(USERNAME, PASSWORD));
	}
}
