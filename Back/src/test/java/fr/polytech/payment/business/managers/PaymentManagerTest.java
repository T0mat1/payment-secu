package fr.polytech.payment.business.managers;

import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import fr.polytech.payment.business.exceptions.ArticleDoesNotExist;
import fr.polytech.payment.business.exceptions.ReferenceAlreadyExists;
import fr.polytech.payment.business.exceptions.ReferenceDoesNotExist;
import fr.polytech.payment.business.exceptions.UserNotFoundBusinessException;
import fr.polytech.payment.mapping.dto.ReferencePanierDTO;
import fr.polytech.payment.mapping.dto.UserDTO;
import fr.polytech.payment.mapping.mappers.impl.ReferencePanierDTOToReferencePanierMapper;
import fr.polytech.payment.mapping.mappers.impl.ReferencePanierToReferencePanierDTOMapper;
import fr.polytech.payment.mapping.mappers.impl.UserToUserDTOMapper;
import fr.polytech.payment.model.Article;
import fr.polytech.payment.model.ReferencePanier;
import fr.polytech.payment.model.User;
import fr.polytech.payment.repositories.ArticleRepository;
import fr.polytech.payment.repositories.PanierRepository;
import fr.polytech.payment.repositories.UserRepository;
import fr.polytech.payment.util.ArticleFactory;
import fr.polytech.payment.util.TestUtils;

@RunWith(MockitoJUnitRunner.class)
@TestInstance(Lifecycle.PER_CLASS)
class PaymentManagerTest {

	private static final String DEFAULT_FAIL_MESSAGE = "No exception must have been caught here.";
	private static final User USER = new User("username", "password");
	private static final Integer MAX_QUANTITY = Integer.valueOf(10);
	private static final Integer MAX_BASKET_LENGTH = Integer.valueOf(10);
	
	private List<ReferencePanier> randomBasket;
	private UserDTO userData;
	private BigDecimal cost;
	private ReferencePanier randomReference;
	private ReferencePanierDTO randomReferenceDto;
	
	private ReferencePanierToReferencePanierDTOMapper entityToDto;
	private UserToUserDTOMapper userToUserDto;
	
	@Autowired
	@InjectMocks
	private PaymentManager manager;
	
	@Mock
	private ArticleRepository articleRepository;
	
	@Mock
	private PanierRepository panierRepository;
	
	@Mock
	private UserRepository userRepository;
	
	
	@BeforeAll
	void onSetUp() {
		MockitoAnnotations.initMocks(this);
		randomBasket = new ArrayList<>();

		entityToDto = new ReferencePanierToReferencePanierDTOMapper();
		new ReferencePanierDTOToReferencePanierMapper();
		
		userToUserDto = new UserToUserDTOMapper();
		
	}
	
	@BeforeEach
	void setUp() {
		
		userData = userToUserDto.map(USER);
		cost = BigDecimal.ZERO;
		
		randomReference = new ReferencePanier();
		randomReference.setArticle(ArticleFactory.createNewRandomArticle());
		randomReference.setOwner(USER);
		randomReference.setQuantity(TestUtils.randomInteger(MAX_QUANTITY-1)+1);
		randomReferenceDto = entityToDto.map(randomReference);
		
		for (int i=0; i<TestUtils.randomInteger(MAX_BASKET_LENGTH)+1; i++) {
			ReferencePanier reference = new ReferencePanier();
			ReflectionTestUtils.setField(reference, "id", UUID.randomUUID());
			reference.setArticle(ArticleFactory.createNewRandomArticle());
			reference.setOwner(USER);
			reference.setQuantity(TestUtils.randomInteger(MAX_QUANTITY-1)+1); // exclude zero
			cost = cost.add(reference.getArticle().getPrice().multiply(BigDecimal.valueOf(reference.getQuantity())));
			randomBasket.add(reference);
		}
	}
	
	@AfterEach
	void cleanUpEach() {
		randomBasket.clear();
	}

	@Test
	void updateBasketOK() throws UserNotFoundBusinessException {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(randomBasket).when(panierRepository).findAllByOwner(USER);
		List<ReferencePanierDTO> result = manager.updateBasket(userData);
		Assertions.assertEquals(randomBasket.size(), result.size());
		for (ReferencePanierDTO ref : result) {
			Assertions.assertTrue(randomBasket.stream()
					.map(reference -> entityToDto.map(reference))
					.anyMatch(reference -> reference.equals(ref)));
		}
	}
	
	@Test
	void updateBasketKO() {
		Mockito.doReturn(Optional.empty()).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Assertions.assertThrows(UserNotFoundBusinessException.class,() -> manager.updateBasket(userData));
	}

	@Test
	void emptyBasketOK() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		try {
			manager.emptyBasket(userData);
		} catch (UserNotFoundBusinessException e) {
			fail(DEFAULT_FAIL_MESSAGE, e);
		}
	}
	
	@Test
	void emptyBasketKO() {
		Mockito.doReturn(Optional.empty()).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Assertions.assertThrows(UserNotFoundBusinessException.class,() -> manager.emptyBasket(userData));
	}

	@Test
	void checkBasketOK() throws UserNotFoundBusinessException {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(randomBasket).when(panierRepository).findAllByOwner(USER);
		randomBasket.stream().findAny().get().getArticle().setStock(Integer.valueOf(0)); //set the value of the stock of an article to zero
		List<ReferencePanier> result = manager.checkBasket(userData);
		Assertions.assertEquals(randomBasket.size()-1, result.size());
		Assertions.assertTrue(result.stream().allMatch(ref -> ref.getQuantity() < ref.getArticle().getStock()));
	}
	
	@Test
	void checkBasketKO() {
		Mockito.doReturn(Optional.empty()).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Assertions.assertThrows(UserNotFoundBusinessException.class,() -> manager.checkBasket(userData));
	}

	@Test
	void processCostOK() {
		Assertions.assertEquals(cost, manager.processCost(randomBasket));
	}

	@Test
	void processPaymentOK() {
		try {
			manager.processPayment(randomBasket);
		} catch (IllegalStateException e) {
			fail(DEFAULT_FAIL_MESSAGE, e);
		}
	}
	
	@Test
	void processPaymentStockKO() {
		randomBasket.stream().findAny().get().getArticle().setStock(Integer.valueOf(0)); //set the value of the stock of an article to zero
		Assertions.assertThrows(IllegalStateException.class, () -> manager.processPayment(randomBasket));
	}
	
	@Test
	void processPaymentUserKO() {
		randomBasket.stream().findAny().get().setOwner(new User("Wrong", "user")); //change the value of a user
		Assertions.assertThrows(IllegalStateException.class, () -> manager.processPayment(randomBasket));
	}

	@Test
	void addReferenceOK() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		try {
			manager.addReference(randomReferenceDto);
		} catch (Exception e) {
			fail(DEFAULT_FAIL_MESSAGE, e);
		}
	}
	
	@Test
	void addReferenceUserNotFoundKO() {
		Mockito.doReturn(Optional.empty()).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(UserNotFoundBusinessException.class, () -> manager.addReference(randomReferenceDto));
	}
	
	@Test
	void addReferenceArticleDoesNotExistKO() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(ArticleDoesNotExist.class, () -> manager.addReference(randomReferenceDto));
	}
	
	@Test
	void addReferenceAlreadyExistsKO() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(ReferenceAlreadyExists.class, () -> manager.addReference(randomReferenceDto));
	}
	
	@Test
	void removeReferenceOK() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		try {
			manager.removeReference(randomReferenceDto);
		} catch (Exception e) {
			fail(DEFAULT_FAIL_MESSAGE, e);
		}
	}

	@Test
	void removeReferenceDoesNotExistKO() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(ReferenceDoesNotExist.class, () -> manager.removeReference(randomReferenceDto));
	}
	
	@Test
	void removeReferenceUserNotFoundKO() {
		Mockito.doReturn(Optional.empty()).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(UserNotFoundBusinessException.class, () -> manager.removeReference(randomReferenceDto));
	}
	
	@Test
	void removeReferenceArticleDoesNotExistKO() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(ArticleDoesNotExist.class, () -> manager.removeReference(randomReferenceDto));
	}

	@Test
	void updateReferenceOK() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		try {
			manager.updateReference(randomReferenceDto);
		} catch (Exception e) {
			fail(DEFAULT_FAIL_MESSAGE, e);
		}
	}
	
	@Test
	void updateReferenceDoesNotExistKO() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(ReferenceDoesNotExist.class, () -> manager.updateReference(randomReferenceDto));
	}
	
	@Test
	void updateReferenceUserNotFoundKO() {
		Mockito.doReturn(Optional.empty()).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(ArticleFactory.createNewRandomArticle())).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(UserNotFoundBusinessException.class, () -> manager.updateReference(randomReferenceDto));
	}
	
	@Test
	void updateReferenceArticleDoesNotExistKO() {
		Mockito.doReturn(Optional.of(USER)).when(userRepository).findByLogin(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.empty()).when(articleRepository).findByReference(ArgumentMatchers.anyString());
		Mockito.doReturn(Optional.of(randomReference)).when(panierRepository).findByOwnerAndArticle(ArgumentMatchers.any(), ArgumentMatchers.any());
		Assertions.assertThrows(ArticleDoesNotExist.class, () -> manager.updateReference(randomReferenceDto));
	}

	@Test
	void isArticleAvailableTrueOK() {
		Article article = ArticleFactory.createNewRandomArticle();
		Integer quantity = article.getStock() - (TestUtils.randomInteger(5)+1); // make sure quantity is less than stock
		Assertions.assertTrue(manager.isArticleAvailable(article, quantity));
	}
	
	@Test
	void isArticleAvailableFalseOK() {
		Article article = ArticleFactory.createNewRandomArticle();
		Integer quantity = article.getStock() + TestUtils.randomInteger(5);
		Assertions.assertFalse(manager.isArticleAvailable(article, quantity));
	}

}
