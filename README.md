# payment-secu

This is the repository for Security course of Polytech Tours Computer Science Department.

## Frontend

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Backend

Maven is required to compile this project.
Java 11 is required to launch this SpringBoot application.
To run the application a PostgreSQL database named "payment" is needed.

All API endpoints are listed in a text file with a Python script in the directory /utils of this repository.
