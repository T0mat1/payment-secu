"""
Parsing script to get all endpoint from a java spring app
Author : Thomas Rossi
Version 1.0
"""

import os

DIRECTORY = "..\\Back\\src\\main\\java\\fr\\polytech\\payment\\api"
ROOT_URL = "http://localhost:8080/v1"
SPRING_SECURITY_LOGIN_PATH = ROOT_URL+"/login"

OUTPUT_FILE_NAME = "SpringAPI_endpoints.txt"


file_list = []

def getAllFiles(directory):
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if os.path.splitext(filename)[-1] == ".java":
                file_list.append(os.path.join(root, filename))

def parseFile(filename):
    root = ""
    paths = []
    methods = []
    deprecated = False
    with open(filename, 'r') as file:
        for line in file.readlines():
            if "@RequestMapping" in line:
                root = line[line.find("(")+2:line.find(")")-1]
            elif "@GetMapping" in line:
                paths.append(line[line.find("(")+2:line.find(")")-1])
                methods.append("GET")
            elif "@PostMapping" in line:
                paths.append(line[line.find("(")+2:line.find(")")-1])
                methods.append("POST")
            elif "@PutMapping" in line:
                paths.append(line[line.find("(")+2:line.find(")")-1])
                methods.append("PUT")
            elif "@DeleteMapping" in line:
                paths.append(line[line.find("(")+2:line.find(")")-1])
                methods.append("DELETE")
            if deprecated:
                methods[-1] = "DEPRECATED"
            deprecated = "@Deprecated" in line
                
    return root, paths, methods
            

if __name__ == "__main__":
    getAllFiles(DIRECTORY)
    outputfile = open(OUTPUT_FILE_NAME, 'w')
    print("Write result in file: {}".format(OUTPUT_FILE_NAME))
    for filename in file_list:
        root, paths, methods = parseFile(filename)
        for i, path in enumerate(paths):
            outputfile.write("{0:60} {1}\n".format(ROOT_URL+root+path, methods[i]))
    if SPRING_SECURITY_LOGIN_PATH is not None and SPRING_SECURITY_LOGIN_PATH != "":
        outputfile.write("{0:60} {1}\n".format(SPRING_SECURITY_LOGIN_PATH, "POST"))
    outputfile.close()
    
